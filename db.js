/**********************************************************************************************
                ******* this file handle the firbase admin sdk *******

    1. create admin instance from firebase-admin moudle in package json
    2. create serviceAccount from json file in the same directory
    3. use winstion Logger to console log info (if the db connection is succesed)
    4. init the admin instance with the service accounet
    5. connect to firestore 
    6. create logRef to save all the logs in firestore "logs" collection
    7. create serviceLogRef to save all the service logs in firebase "servicelogs" collection
    8. export "db, admin, logRef, serviceLogRef" to be available throughout the whole app

***********************************************************************************************/

var admin = require("firebase-admin");
var serviceAccount = require("./vpn-pro-3f62c-firebase-adminsdk-gl8vd-455aae70c1.json");
 
// console.log(serviceAccount)
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
});
  const uid = "tamir-uid";
  const additionalClaims = {
    premiumAccount: true,
  };

  const db = admin.firestore();
  const vpnServersNodataEncryptRef = db.collection("vpn-servers-nodata-encrypt");
  const vpnServersEncryptRef = db.collection("vpn-servers-encrypt");

// firebase.firestore.Timestamp.now();
if (vpnServersNodataEncryptRef) {
  console.log("DB connected!"); 
} else {
  console.log("DB not connected!");
} 

module.exports = { db, admin, vpnServersNodataEncryptRef, vpnServersEncryptRef }; 

///////  testing  exemple ////////////////////////////////////////////////////////////////////////
// const data = {
//   name: "test3",
//   status: "ok",
// };

// Ref.doc("dHAUBpT0jCBoylL6PZUv")
//   .get()
//   .then((doc) => {
//     if (!doc.exists) {
//       console.log("doc is not exists");
//     }
//     console.log(doc.data());
//   })
//   .catch((error) => {
//     console.log(error);
//   });



// Ref.doc.get()
//   .then((doc) => {
//     if (!doc.exists) {
//       console.log("doc is not exists");
//     }
//     console.log(doc.data()) 
//   })
//   .catch((error) => {
//     console.log(error);
//   });
// ref.set(data).then(() => {
//   console.log("new data added to testCollection");
// });
// const getAll = async (req, res, next) => {
//   try {
//     const data = await ref.get();
//     console.log(data);
//     // res.send(data);
//   } catch (error) {
//     console.log(error);
//     // res.status(400).send(error.message);
//   }
// };
// const result = await getAll();
// console.log(result)




// admin
//   .auth()
//   .createCustomToken(uid, additionalClaims)
//   .then((customToken) => {
//     console.log("token is ready");
//   })
//   .catch((error) => {
//     console.log("error creating custom token ", error);
//   });
