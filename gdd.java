package com.omshyapps.vpn;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;

import com.omshyapps.smartvpn.model.Server;
import com.omshyapps.vpn.appstat.pojos.VpnConfigurationPojo;
import com.omshyapps.vpn.region.RegionManager;
import com.omshyapps.vpn.utils.OmshyAppsMenuDialog;

import java.util.ArrayList;


/**
 * Provides a bottom sheet menu dialog box whose content is provided by a menu resource.
 */
public class CountrySelectionActivity extends OmshyAppsMenuDialog implements ListView.OnItemClickListener
{

    protected ArrayAdapter<Server> mAdapter;
    protected ImageView mTitleImageView = null;
    protected TextView mTitleTextView = null;
    protected com.omshyapps.vpn.utils.CountrySelectionDialog.OnCountryClickListener mClickListener = null;
    protected Server currentServer;

    public interface OnCountryClickListener
    {
        public void onItemClick(Server country);
    }

    public void setOnItemClickListener(com.omshyapps.vpn.utils.CountrySelectionDialog.OnCountryClickListener listener)
    {
        mClickListener = listener;
    }

    public CountrySelectionActivity(@NonNull Context context, ArrayList<Server> countryList, Server currentServer)
    {
        super(context, R.layout.bottom_sheet_menu_layout);
        initialize(context, countryList, currentServer);
    }

    public CountrySelectionActivity(@NonNull Context context, ArrayList<Server> countryList, @StringRes int titleId, Server currentServer)
    {
        super(context, R.layout.bottom_sheet_menu_layout, titleId);
        initialize(context, countryList, currentServer);
    }

    protected void initialize(Context context, ArrayList<Server> countryList, Server currentServer)
    {
        
        String BEST_CONNECTION = "Best Connection";
        mTitleImageView = (ImageView) findViewById(R.id.bottom_sheet_menu_title_image);
        mTitleTextView = (TextView) findViewById(R.id.bottom_sheet_menu_title_text);
        ListView listView = (ListView) findViewById(R.id.bottom_sheet_menu_list_view);
        String currentDefault = PreferenceManager.getDefaultSharedPreferences(getContext()).getString(AppConfig.DefaultRegion, BEST_CONNECTION);
        VpnConfigurationPojo vpnConfigurationPojo = RegionManager.getInstance(context).getTheBestServerConnection(context);
        Server bestConnectionServer = new Server("", "", "", "", "", vpnConfigurationPojo.getCountryLong(), BEST_CONNECTION, "0", "",
                "0", "0", "", "", "", "", 0, "", 0, "", "", 0, 0, 0, "", "", "", "", "", "");


        ArrayList<Server> sortedList = new ArrayList<>();
        if (sortedList.size() == 0)
        {
            sortedList.addAll(countryList);
        }

        if(currentDefault.equals(BEST_CONNECTION) || sortedList.isEmpty()){
            sortedList.add(0, bestConnectionServer);
        }
        else
            sortedList.add(1, bestConnectionServer);

        mAdapter = new ArrayAdapter<Server>(context, 0, sortedList)
        {
            @Override
            public View getView(int position, View convertView, ViewGroup parent)
            {
                final Server country = getItem(position);
                View newView;

                newView = getLayoutInflater().inflate(R.layout.country_selection_item_layout, null);
                String code = country.getCountryShort().toLowerCase();
                if (code.equals("do"))
                    code = "dom";

                if(code.equals(BEST_CONNECTION.toLowerCase())){
                    ImageView img =  newView.findViewById(R.id.image_view);
                    img.setImageResource(
                            context.getResources().getIdentifier("ic_speed_24px",
                                    "drawable", context.getPackageName()));
                    img.getLayoutParams().width = 144;
                    ((TextView) newView.findViewById(R.id.text_view)).setText("Best connection" + " (" + vpnConfigurationPojo.getCountryLong() + ")");
                }
                else {
                    ((ImageView) newView.findViewById(R.id.image_view)).setImageResource(
                            context.getResources().getIdentifier(code,
                                    "drawable",
                                    getContext().getPackageName()));
                    ((TextView) newView.findViewById(R.id.text_view)).setText(country.getCountryLong());

                }
                newView.setAlpha(1.0f);//item.isEnabled() ? 1.0f : 0.5f);

                //mark the current selected country
                if (currentServer != null &&
                        code.equalsIgnoreCase(currentDefault)) {

                    //mark current connection
                    ((TextView) newView.findViewById(R.id.text_view)).setTextColor(Color.parseColor("#285799"));
                    Typeface boldTypeface = Typeface.defaultFromStyle(Typeface.BOLD);
                    ((TextView) newView.findViewById(R.id.text_view)).setTypeface(boldTypeface);
                }

                //set connection signal
                if (Integer.valueOf(country.getNumVpnSessions()) > 20)
                {
                    ((ImageView) newView.findViewById(R.id.connection_signal)).setImageResource(R.drawable.ic_connect_good);
                } else if (Integer.valueOf(country.getNumVpnSessions()) > 30)
                {
                    ((ImageView) newView.findViewById(R.id.connection_signal)).setImageResource(R.drawable.ic_connect_bad);
                }
                else {
                    ((ImageView) newView.findViewById(R.id.connection_signal)).setImageResource(R.drawable.ic_connect_excellent);
                }

                return newView;
            }
        };

        listView.setAdapter(mAdapter);
        listView.setOnItemClickListener(this);
    }

    public ImageView getTitleImageView()
    {
        return mTitleImageView;
    }

    public TextView getTitleTextView()
    {
        return mTitleTextView;
    }

    public void refresh()
    {
        //mAdapter.clear();
        //mAdapter.addAll(mMenuBuilder.getVisibleItems());
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        Server country = mAdapter.getItem(position);
        if (mClickListener != null)
        {
            mClickListener.onItemClick(country);
        }

        dismiss();
    }
}