// const fs = require('fs');
// const path = require('path');

const sendMail = require('./gmail')
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const redis = require('redis')
const chalk = require('chalk')


const { db, vpnServersNodataEncryptRef, vpnServersEncryptRef } = require('./db')
const app = express()
// app.use(express.urlencoded({ extended: true }));
// app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(cors())
const DEFAULT_EX = 180
// const YELLOW = '\x1b[33m%s\x1b[0m'
// const GREEN = "\x1b[34m%s\x1b[0m"
// const RED = "\x1b[31m%s\x1b[0m"


let redisClient;
(async () => {
  redisClient = redis.createClient()
  redisClient.on('error', (error) => console.error(`Error : ${error}`))
  await redisClient.connect()
})()

const main = async (massage) => {
  //   const fileAttachments = [
  //     {
  //       filename: 'attachment1.txt',
  //       content: 'This is a plain text file sent as an attachment',
  //     },
  //     {
  //       path: path.join(__dirname, './attachment2.txt'),
  //     },
  //     {
  //       filename: 'websites.pdf',
  //       path: 'https://www.labnol.org/files/cool-websites.pdf',
  //     },

  //     {
  //       filename: 'image.png',
  //       content: fs.createReadStream(path.join(__dirname, './attach.png')),
  //     },
  //   ];

  const options = {
    to: massage.email,
    // cc: 'cc1@example.com, cc2@example.com',
    // replyTo: 'amit@labnol.org',
    subject: massage.subject,
    text: massage.body,

    // html: `<p>🙋🏻‍♀️  &mdash; This is a <b>test email</b> from <a href="https://digitalinspiration.com">Digital Inspiration</a>.</p>`,
    // attachments: fileAttachments,
    textEncoding: 'base64',
    headers: [
      { key: 'X-Application-Developer', value: 'Vpn desoline' },
      { key: 'X-Application-Version', value: 'v1.0.0.2' },
    ],
  }

  const messageId = await sendMail(options)
  return messageId
}

// main()
//   .then((messageId) => console.log('Message sent successfully:', messageId))
//   .catch((err) => console.error(err));

app.post('/sendEmail', async (request, response) => {
  console.log('request sendEmail: ', request.body)
  const massage = request.body
  console.log(request.body)
  if (massage) {
    const massageId = await main(massage)
      .then((massageId) => {
        console.log('Message sent successfully:', massageId)
        response.status(200)
        response.send({ massage: 'Email sent successfully' })
      })
      .catch((err) => {
        console.error(err)
        response.send({ massage: err })
      })
  } else {
    // 201 is CREATED
    response.send({ massage: 'bad request' })
  }
})

app.post(
  '/conversion/servers',
  serversCacheMiddeleware,
  async (request, response) => {
    // console.log( YELLOW'post request: /conversion/conversion/servers'))
    try {
      let data
      const snapshot = await db.collection('vpn-servers-nodata-encrypt').get()
      data = snapshot.docs.map((doc) => doc.data())
      
      if (data) {
        data.forEach((doc) => {
          doc.creationTime = null
        })
        redisClient.setEx(`servers`, DEFAULT_EX, JSON.stringify(data))
        console.log(chalk.yellow( 'servers request succeeded'))
        response.json(data)
      } else {
        response.status(400)
        return response.send(error)
      }
    } catch (error) {
      console.log(chalk.red('error servers: ', error))
      response.status(400)
      return response.send(error)
    }

    // let data;
    // try {
    //   const snapshot = await db.collection('vpn-servers-nodata-encrypt').get()
    //   data = snapshot.docs.map((doc) => doc.data())
    //   // console.log("****************************************************************")
    //   data.forEach(doc => {
    //     // console.log("countryShort:   ", doc.countryShort, ",  id: ", doc.id)
    //     doc.creationTime = null
    //   });

    // } catch (error) {
    //   console.log('error: ', error)
    //   response.status(400)
    //   return response.send(error)
    // }
    // if (data) {
    //   console.log('requset OK data.length:', data.length)
    //   response.status(200)
    //   return response.send(data)
    // }
  },
)

async function serversCacheMiddeleware(req, res, next) {
  console.log( chalk.yellow( 'post request: *** servers ***'))
  const data = await redisClient.get(`servers`)
  
  if (data !== null) {
    console.log(chalk.yellow( 'servers cache hit'))
    return res.json(JSON.parse(data))
  } else {
    console.log(chalk.yellow( 'servers cache mis'))
    next()
  }
}

async function serverIdCache(req, res, next) {
  console.log(chalk.green( 'post request: **  serverId  **'))
  console.log( chalk.green( 'firebaseDocId: ', req.body.firebaseDocId))

  const firebaseDocId = req.body.firebaseDocId
  const data = await redisClient.get(firebaseDocId)
  if (data !== null) {
    console.log(chalk.green( 'serverId cache hit'))
    return res.json(JSON.parse(data))
  } else {
    console.log(chalk.green( 'serverId cache mis'))
    next()
  }
}

app.post('/conversion/server/data', serverIdCache,  async (request, response) => {

  const firebaseDocId = request.body.firebaseDocId
  if (!firebaseDocId) {
    console.log(chalk.red('Bad request!'))
    return response.status(401).send('Bad request!')
  }
  let docRef
  let doc
  try {
    // cheack if there is doc with session id in the sessions collection
    doc = await db.collection('vpn-servers-encrypt').doc(firebaseDocId)
    docRef = await doc.get()
    // docRef.data()
  } catch (error) {
    console.log(chalk.red('error serverId: ', error))
    response.status(400)
    response.send(error)
  }

  if (!docRef.data()) {
    console.log( chalk.green('docRef undefined :'+ docRef.data()))
    console.log(chalk.green('docRef in request body: ', firebaseDocId))
    //   todo
    const validDocRefId = await setIdToUndefinedId(firebaseDocId)
    if(validDocRefId)
      return response.send(validDocRefId)

    response.status(400)
    return response.send('error *** setIdToUndefinedId function faild')
  }
  if (docRef.data()) {
    console.log(chalk.green('docRef ok id:', docRef.id))
    redisClient.setEx(firebaseDocId, 50000, JSON.stringify(docRef.data()))
    response.status(200)
    response.send(docRef.data())
  }
})

async function setIdToUndefinedId(id){
  const data = await redisClient.get(`servers`)
  let validId;
  let doc;
  let docRef;
  if (data !== null) {
    let size = (JSON.parse(data).length)
    console.log(chalk.red("size from cache: ", size))
    size-=1;
    let num = Math.floor(Math.random() * (size - 1));
    validId = JSON.parse(data)[num].id;
    console.log(chalk.green(`rundom server number from cache: ${num}, id: ${validId}`))
    doc = await db.collection('vpn-servers-encrypt').doc(validId)
    docRef = await doc.get()
    redisClient.setEx(id, 50000, JSON.stringify(docRef.data()))
    
  }
  else{
    const snapshot = await db.collection('vpn-servers-nodata-encrypt').get()
    const tempList =  snapshot.docs.map((doc) => doc.data())
    let size = tempList.length
    console.log(chalk.red("size from DB: ", size))
    size-=1;
    let num = Math.floor(Math.random() * (size - 1));
    console.log(chalk.red(`rundom server number from FB: ${num}`))
    doc = await db.collection('vpn-servers-encrypt').doc(tempList[num].id)
    docRef = await doc.get()
    redisClient.setEx(id, 50000, JSON.stringify(docRef.data()))
  }
  return docRef.data();
}

app.get('/alive', (request, response) => {
  response.status(200) // 200 is OK
  response.send(`server is up!!!`)
  console.log('server is up!!!: ok')
})

app.get('/test', (request, response) => {
  response.status(200) // 200 is OK
  response.send(`server is up!!!`)
  console.log('server is up!!!: ok')
})


app.listen(8200, () => {
  console.log('Server is running in port 8200')
})

